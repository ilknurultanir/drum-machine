import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import KeyboardEventHandler from 'react-keyboard-event-handler';

class Drums extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventKey: ""
        }
        this.handlePress = this
            .handlePress
            .bind(this);
        this.handleClick = this
            .handleClick
            .bind(this);
        this.onKeyPressed = this
            .onKeyPressed
            .bind(this);
    }
    handleClick(e) {
        this.setState({eventKey: e.target.id})
        document
            .getElementById(e.target.id)
            .getElementsByClassName("clip")[0]
            .play();
        document
            .getElementById(e.target.id)
            .getElementsByClassName("clip")[0]
            .addEventListener("ended", function () {
                this.currentTime = 0;
                console.log("ended");
                this.removeEventListener("click",()=>{
                    console.log("ended*2");
                });
            });
    }
    handlePress(key, e) {
        this.setState({eventKey: key})
        document
            .getElementById(key.toUpperCase())
            .getElementsByClassName("clip")[0]
            .play();
    }
    onKeyPressed(e) {
        switch (e.key.toLowerCase()) {
            case "q":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "w":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "e":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "a":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "s":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "d":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "z":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "x":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            case "c":
                document
                    .getElementById(e.key.toUpperCase())
                    .click();
                break;
            default:
                console.log("Sorry for all the lols and lels");

        }
    }
    render() {
        return (
            <Container
                id="drum-machine"
                className="lol"
                tabIndex="1"
                onKeyDown={(e) => this.onKeyPressed(e)}>
                <Row>
                    <Col sm="12">
                        <a
                            className="drum-pad"
                            id="Q"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>Q
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="Q" className="clip"></audio>
                        </a>
                        <a
                            className="drum-pad"
                            id="W"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>W
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="W" className="clip"></audio>
                        </a>
                        <a
                            className="drum-pad"
                            id="E"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>E
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="E" className="clip"></audio>
                        </a>
                    </Col>
                </Row>
                <Row>
                    <Col sm="12">
                        <a
                            className="drum-pad"
                            id="A"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>A
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="A" className="clip"></audio>
                        </a>
                        <a
                            className="drum-pad"
                            id="S"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>S
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="S" className="clip"></audio>
                        </a>
                        <a
                            className="drum-pad"
                            id="D"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>D
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="D" className="clip"></audio>
                        </a>
                    </Col>
                </Row>
                <Row>
                    <Col sm="12">
                        <a
                            className="drum-pad"
                            id="Z"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>Z
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="Z" className="clip"></audio>
                        </a>
                        <a
                            className="drum-pad"
                            id="X"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>X
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="X" className="clip"></audio>
                        </a>
                        <a
                            className="drum-pad"
                            id="C"
                            role="link"
                            color="light"
                            tabIndex="0"
                            onClick={(e) => this.handleClick(e)}>C
                            <audio src="http://vispo.com/vismu/oppen/audio/1.wav" id="C" className="clip"></audio>
                        </a>
                    </Col>
                </Row>
                <div id="display" className="m-5 text-white display-4">{this.state.eventKey}</div>
                <KeyboardEventHandler
                    handleKeys={[
                    'z',
                    'x',
                    'c',
                    'a',
                    's',
                    'd',
                    'q',
                    'w',
                    'e',
                    'a'
                ]}
                    onKeyEvent={(key, e) => this.handlePress(key, e)}/>
            </Container>
        )
    }
}

export default Drums;