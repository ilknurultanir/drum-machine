import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this
            .toggle
            .bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                <Navbar light className="fixed-bottom text-white" expand="md">
                    <NavbarBrand href="/" className="text-white">Drummer for #fcc
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle}/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink
                                    rel="noopener noreferrer"
                                    target="_blank"
                                    href="//bitbucket.org/ilknurultanir/drum-machine/src">Check this project on Bitbucket</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    rel="noopener noreferrer"
                                    target="_blank"
                                    href="//github.com/iibarbari">My GitHub</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default NavBar;