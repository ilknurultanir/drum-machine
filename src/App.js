import React, { Component } from 'react';
import './App.css';
import Drums from './Components/Drums';
import NavBar from './Components/Navbar';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <Drums />
      </div>
    );
  }
}

export default App;
